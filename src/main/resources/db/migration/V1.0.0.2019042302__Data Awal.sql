insert into wallet (id, id_user, name) values
('w001', 'user001', 'Wallet User 001');

insert into wallet (id, id_user, name) values
('w002', 'user002', 'Wallet User 002');

insert into wallet_transaction(id, id_wallet, transaction_time, amount, description, reference) VALUES
('wt001', 'w001', '2019-04-23 08:00:00', 100000.00, 'Top Up Saldo', 'abcd123401');

insert into wallet_transaction(id, id_wallet, transaction_time, amount, description, reference) VALUES
('wt002', 'w001', '2019-04-23 09:00:00', -10000.00, 'Beli pulsa', 'abcd123402');